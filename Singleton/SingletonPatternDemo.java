package Singleton;

public class SingletonPatternDemo {
    public static void main(String[] args){
        SingleObject singleton = SingleObject.getInstance();
        singleton.showMessage();
    }
}
