package Singleton;

public class SingleObject {
    private static SingleObject singleton = new SingleObject();
    private SingleObject(){};
    public static SingleObject getInstance(){
        return singleton;
    }
    public void showMessage(){
        System.out.println("singleton");
    }
}
