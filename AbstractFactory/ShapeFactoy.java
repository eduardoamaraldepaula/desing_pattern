package AbstractFactory;

public class ShapeFactoy extends AbstractFactory {
    public Shape getShape(String forma){
        if(forma=="Square"){
            return new Square();
        } else {
            return new Rectangle();
        }
    }
}
