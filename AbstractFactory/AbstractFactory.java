package AbstractFactory;

abstract class AbstractFactory {
    public abstract Shape getShape(String forma);
}
