package AbstractFactory;

public class RoundedShapeFactoy extends AbstractFactory{
    public Shape getShape(String forma){
        if(forma=="Rectangle"){
            return new RoundedRectagle();
        } else{
            return new RoundedSquare();
        }
    }
}
