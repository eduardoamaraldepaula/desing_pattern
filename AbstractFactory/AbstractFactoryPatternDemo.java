package AbstractFactory;

public class AbstractFactoryPatternDemo {
    public static void main(String[] args){
        factoryProducer obj = new factoryProducer();
        AbstractFactory fabrica = obj.getFactory(false);
        Shape forma = fabrica.getShape("Rectangle");
        forma.draw();
        
    }
}
