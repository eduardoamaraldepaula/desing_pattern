public class ShapeFactory {
    public Shape getShape(String forma){
        if(forma=="Cicle"){
            return new Cicle();
        } else if(forma=="Square"){
            return new Square();
        } else {
            return new Rectangle();
        }
    }

}
