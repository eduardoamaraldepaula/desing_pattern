public class Factory{
    
    public static void main(String[] args){
        ShapeFactory obj = new ShapeFactory();
        Shape forma = obj.getShape("Cicle");
        forma.draw();
    }
}